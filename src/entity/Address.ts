import { Column } from "typeorm";

export class Address {
    @Column()
    street: string;

    @Column()
    suite: string;

    @Column()
    city: string;

    @Column()
    zipcode: string;
}
