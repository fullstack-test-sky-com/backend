import { Entity, ObjectID, ObjectIdColumn, Column } from "typeorm";
import { Address } from "./Address";

@Entity()
export class User {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    name: string;

    @Column()
    username: string;

    @Column()
    email: string;

    @Column()
    address: Address;

    @Column()
    phone: string;

    @Column()
    website: string;
}
