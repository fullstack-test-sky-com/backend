import { getMongoManager } from "typeorm";
import { Request, Response } from "express";
import { ObjectID } from "mongodb";
import { User } from "../entity/User";
import { Address } from "../entity/Address";

export class UserController {
    private userRepository = getMongoManager();

    async all(request: Request, response: Response) {
        try {
            const result = await this.userRepository.findAndCount(User, {});
            if (result) {
                const [data, count] = result;
                return response.status(200).send({
                    response: { data: data, amount: count },
                    status: true,
                });
            }
            return response.status(400).send({ status: false });
        } catch (error) {
            return response.status(400).send({ status: false });
        }
    }

    async one(request: Request, response: Response) {
        try {
            const reqParams = request.params;
            if (!reqParams.id) {
                return response.status(400).send({ status: false });
            }

            const id = reqParams.id;

            const result: any = await this.userRepository.findOne(User, {
                _id: ObjectID(id),
            });

            if (result) {
                return response.status(200).send({
                    response: result,
                    status: true,
                });
            }

            return response.status(400).send({ status: false });
        } catch (error) {
            return response.status(400).send({ status: false });
        }
    }

    async save(request: Request, response: Response) {
        try {
            const reqBody = request.body;
            if (!reqBody) {
                return response.status(400).send({ status: false });
            }

            const user = new User();
            user.name = reqBody.name;
            user.username = reqBody.username;
            user.email = reqBody.email;
            user.address = new Address();
            user.address.street = reqBody.address.street;
            user.address.suite = reqBody.address.suite;
            user.address.city = reqBody.address.city;
            user.address.zipcode = reqBody.address.zipcode;
            user.phone = reqBody.phone;
            user.website = reqBody.website;

            const result = await this.userRepository.save(User, user);
            if (result) {
                return response.status(200).send({
                    response: result,
                    status: true,
                });
            }
            return response.status(400).send({ status: false });
        } catch (error) {
            return response.status(400).send({ status: false });
        }
    }
    async update(request: Request, response: Response) {
        try {
            const reqParams = request.params;
            if (!reqParams.id) {
                return response.status(400).send({ status: false });
            }
            const reqBody = request.body;
            const id = reqParams.id;

            const user = new User();
            user.name = reqBody.name;
            user.username = reqBody.username;
            user.email = reqBody.email;
            user.address = new Address();
            user.address.street = reqBody.address.street;
            user.address.suite = reqBody.address.suite;
            user.address.city = reqBody.address.city;
            user.address.zipcode = reqBody.address.zipcode;
            user.phone = reqBody.phone;
            user.website = reqBody.website;

            const result = await this.userRepository.findOneAndUpdate(
                User,
                { _id: ObjectID(id) },
                { $set: user }
            );

            if (result.ok == 1) {
                return response.status(200).send({
                    status: true,
                });
            }
            return response.status(400).send({ status: false });
        } catch (error) {
            return response.status(400).send({ status: false });
        }
    }

    async remove(request: Request, response: Response) {
        try {
            const reqParams = request.params;
            if (!reqParams.id) {
                return response.status(400).send({ status: false });
            }

            const id = reqParams.id;

            const result = await this.userRepository.findOne(User, {
                _id: ObjectID(id),
            });

            if (result) {
                await this.userRepository.delete(User, {
                    _id: ObjectID(result._id),
                });
                return response.status(200).send({
                    status: true,
                });
            }

            return response.status(400).send({ status: false });
        } catch (error) {
            return response.status(400).send({ status: false });
        }
    }

    async get(request: Request, response: Response) {
        try {
            const reqQuery = request.query;
            if (!reqQuery.skip && !reqQuery.take) {
                return response.status(400).send({ status: false });
            }
            const keyword = reqQuery.keyword;
            const take = parseInt(reqQuery.take);
            const skip = parseInt(reqQuery.skip);
            const query: any = {};

            const regex = { $regex: keyword, $options: "i" };
            const search = {
                $or: [
                    {
                        name: regex,
                    },
                    {
                        email: regex,
                    },
                ],
            };
            query.where = keyword ? search : {};
            query.skip = skip * take;
            query.take = take;

            const result = await this.userRepository.findAndCount(User, query);

            if (result) {
                const [data, count] = result;
                return response.status(200).send({
                    response: { data: data, amount: count },
                    status: true,
                });
            }

            return response.status(400).send({ status: false });
        } catch (error) {
            return response.status(400).send({ status: false });
        }
    }
}
