import { UserController } from "./controller/UserController";

export const Routes = [
    {
        method: "get",
        route: "/users/all",
        controller: UserController,
        action: "all",
    },
    {
        method: "get",
        route: "/users/:id",
        controller: UserController,
        action: "one",
    },
    {
        method: "get",
        route: "/users",
        controller: UserController,
        action: "get",
    },
    {
        method: "post",
        route: "/users",
        controller: UserController,
        action: "save",
    },
    {
        method: "put",
        route: "/users/:id",
        controller: UserController,
        action: "update",
    },
    {
        method: "delete",
        route: "/users/:id",
        controller: UserController,
        action: "remove",
    },
];
